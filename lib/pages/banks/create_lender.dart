import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:loans_client/shared/authed_route_params.dart';
import 'package:loans_client/shared/widgets/base_page_layout.dart';

import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:loans_client/shared/utils/validators.dart';
import 'package:loans_client/shared/utils/show_snackbar.dart';
import 'package:loans_client/shared/utils/http_helper.dart';

class CreateLenderScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasePageLayout(
      pageTitle: 'Add New Bank',
      body: Card(
        child: LenderCreationForm(),
        elevation: 0.2,
      ),
    );
  }
}

class LenderCreationForm extends StatefulWidget {
  @override
  _LenderCreationFormState createState() => _LenderCreationFormState();
}

class _LenderCreationFormState extends State<LenderCreationForm> {
  AuthedRouteParams? args;
  bool _isSubmitting = false;

  final _formKey = GlobalKey<FormState>();

  final _bankNameController = TextEditingController(text: "");
  final _branchNameController = TextEditingController(text: "");
  final _accountNameController = TextEditingController(text: "");
  final _accountNumberController = TextEditingController(text: "");
  final _openingBalanceController = TextEditingController(text: "");
  final _asOfDateController = TextEditingController(text: "");
  final _currencyController = TextEditingController(text: "");
  String? _selCurrency;
  final _chequeDaysController = TextEditingController(text: "");
  final _odInterestController = TextEditingController(text: "");
  final _odLimitController = TextEditingController(text: "");
  final _odLimitInterestController = TextEditingController(text: "");

  Future _submitForm() async {
    Map<String, String> body = {
      'bank_name': _bankNameController.text,
      'branch': _branchNameController.text,
      'account_name': _accountNameController.text,
      'account_number': _accountNumberController.text,
      'currency': _selCurrency!,
      'cheque_clear_days': _chequeDaysController.text,
      'overdraft_interest': _odInterestController.text,
      'overdraft_limit_interest': _odLimitInterestController.text,
      'overdraft_limit': toNumericString(_odLimitController.text),
    };

    final response = await HttpHelper.sendPostData(args!, 'bank_routes', body);
    if (response.statusCode == 201) {
      // final Map<String, dynamic> responseMap = jsonDecode(response.body);
      _bankNameController.text = "";
      _branchNameController.text = "";
      _accountNameController.text = "";
      _accountNumberController.text = "";
      _openingBalanceController.text = "";
      _asOfDateController.text = "";
      _currencyController.text = "";
      _chequeDaysController.text = "";
      _odInterestController.text = "";
      _odLimitController.text = "";
      _odLimitInterestController.text = "";
      print("${response.statusCode} : ${response.body}");
      ShowSnackbar.success(context, "Record saved");
    } else {
      ShowSnackbar.dangerResponse(context, response.statusCode);
      print("${response.statusCode} : ${response.body}");
    }

    setState(() {
      _isSubmitting = false;
    });

    // print(body);
  }

  @override
  Widget build(BuildContext context) {
    args = ModalRoute.of(context)!.settings.arguments as AuthedRouteParams;
    print("Lender: " + args!.token);

    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 10, left: 10),
            child: Text(
              'Bank Details',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 16,
              ),
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _bankNameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Bank name',
                      labelText: 'Bank Name*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _branchNameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Branch name',
                      labelText: 'Branch Name*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _accountNumberController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Account number',
                      labelText: 'Account Number*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _accountNameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Account name',
                      labelText: 'Account Name*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _openingBalanceController,
                    inputFormatters: [
                      MoneyInputFormatter(),
                    ],
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Opening balance',
                      labelText: 'Opening Balance*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: DateTimeField(
                    controller: _asOfDateController,
                    format: DateFormat('dd-MM-yyyy'),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'As of',
                      labelText: 'As Of Date*',
                    ),
                    validator: Validators.isNonStringFilled,
                    onShowPicker: (context, currentValue) {
                      return showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100),
                      );
                    },
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: DropdownSearch<String>(
                    searchFieldProps: TextFieldProps(
                      controller: _currencyController,
                    ),
                    onChanged: (val) {
                      _selCurrency = val!;
                    },
                    mode: Mode.MENU,
                    showSelectedItem: true,
                    items: ["KES", "\$"],
                    label: "Currency",
                    dropdownSearchDecoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 12),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                      hintText: 'Currency',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _chequeDaysController,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Days',
                      labelText: 'Cheque Clear Days*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _odInterestController,
                    inputFormatters: [
                      MoneyInputFormatter(),
                    ],
                    decoration: InputDecoration(
                      suffixIcon: Padding(
                        padding: EdgeInsets.all(10),
                        child: FaIcon(FontAwesomeIcons.percent, size: 14),
                      ),
                      border: OutlineInputBorder(),
                      hintText: '%',
                      labelText: 'Overdraft Interest*',
                    ),
                    validator: Validators.isPercentage,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _odLimitController,
                    inputFormatters: [
                      MoneyInputFormatter(),
                    ],
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Overdraft limit',
                      labelText: 'Overdraft Limit*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _odLimitInterestController,
                    inputFormatters: [
                      MoneyInputFormatter(),
                    ],
                    decoration: InputDecoration(
                      suffixIcon: Padding(
                        padding: EdgeInsets.all(10),
                        child: FaIcon(FontAwesomeIcons.percent, size: 14),
                      ),
                      border: OutlineInputBorder(),
                      hintText: '%',
                      labelText: 'Overdraft Limit Interest*',
                    ),
                    validator: Validators.isPercentage,
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: ElevatedButton(
              onPressed: () {
                if (_isSubmitting) {
                  return;
                }

                if (_formKey.currentState!.validate()) {
                  // Yay, valid
                  setState(() {
                    _isSubmitting = true;
                  });

                  ShowSnackbar.primary(context, "Saving data");
                  _submitForm();
                }
              },
              child: _isSubmitting
                  ? SizedBox(
                      width: 14,
                      height: 14,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                        strokeWidth: 2,
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Text('Submit'),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
