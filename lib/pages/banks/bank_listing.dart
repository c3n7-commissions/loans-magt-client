import 'package:flutter/material.dart';
import 'dart:math';

import 'package:loans_client/shared/authed_route_params.dart';
import 'package:loans_client/shared/widgets/drawer.dart';

class BankListingScreen extends StatefulWidget {
  @override
  _BankListingScreenState createState() => _BankListingScreenState();
}

class _BankListingScreenState extends State<BankListingScreen> {
  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)!.settings.arguments as AuthedRouteParams;
    print("Token: " + args.token);

    return Scaffold(
      appBar: AppBar(
        title: Text('Qubes'),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 40),
            child: Tooltip(
              message: 'Log out',
              child: IconButton(
                onPressed: () {},
                icon: Icon(Icons.power_settings_new_rounded),
              ),
            ),
          ),
        ],
      ),
      drawer: NavigationDrawer(),
      body: Padding(
        padding: EdgeInsets.all(70),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10, left: 10, bottom: 20),
              child: Text(
                'Banks',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 23,
                ),
              ),
            ),
            Card(
              child: BankListing(),
              elevation: 0.2,
            )
          ],
        ),
      ),
    );
  }
}

class BankListing extends StatefulWidget {
  @override
  _BankListingState createState() => _BankListingState();
}

class _BankListingState extends State<BankListing> {
  DataTableSource _data = MyData();

  @override
  Widget build(BuildContext buildContext) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text('Products'),
        PaginatedDataTable(
          source: _data,
          header: Text('My Products'),
          columns: [
            DataColumn(label: Text('ID')),
            DataColumn(label: Text('Name')),
            DataColumn(label: Text('Price'))
          ],
          columnSpacing: 100,
          horizontalMargin: 10,
          rowsPerPage: 8,
          showCheckboxColumn: false,
        ),
      ],
    );
  }
}

// The "soruce" of the table
class MyData extends DataTableSource {
  // Generate some made-up data
  final List<Map<String, dynamic>> _data = List.generate(
      200,
      (index) => {
            "id": index,
            "title": "Item $index",
            "price": Random().nextInt(10000)
          });

  bool get isRowCountApproximate => false;
  int get rowCount => _data.length;
  int get selectedRowCount => 0;
  DataRow getRow(int index) {
    return DataRow(cells: [
      DataCell(Text(_data[index]['id'].toString())),
      DataCell(Text(_data[index]["title"])),
      DataCell(Text(_data[index]["price"].toString())),
    ]);
  }
}
