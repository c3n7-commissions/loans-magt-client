import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:loans_client/shared/authed_route_params.dart';
import 'dart:async';
import 'package:http/http.dart' as http;

import 'package:loans_client/shared/widgets/base_page_layout.dart';

class CompanyListingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasePageLayout(
      pageTitle: 'Manage Company',
      body: Card(
        elevation: 0.2,
        child: CompanyListingTable(),
      ),
    );
  }
}

class CompanyListingTable extends StatefulWidget {
  @override
  _CompanyListingTableState createState() => _CompanyListingTableState();
}

class _CompanyListingTableState extends State<CompanyListingTable> {
  AuthedRouteParams? args;

  late Future<DataTableSource> _futureData;
  Future<DataTableSource> _getData() async {
    final response = await http.get(
        Uri.parse(
            args!.config['server_address'] + args!.config['company_routes']),
        headers: <String, String>{
          'Authorization': 'Bearer ' + args!.token,
        });

    print("${response.statusCode} : ${response.body}\n\n");
    DataTableSource? data;

    if (response.statusCode == 200) {
      final Map<String, dynamic> responseJson = jsonDecode(response.body);
      final List<Map<String, dynamic>> parsedResponse = [];
      responseJson['data'].forEach((element) {
        final row = Map<String, dynamic>.from(element);
        parsedResponse.add(row);
      });

      print(parsedResponse);
      data = new MyData(data: parsedResponse);
    }

    return data!;
  }

  @override
  Widget build(BuildContext context) {
    args = ModalRoute.of(context)!.settings.arguments as AuthedRouteParams;
    _futureData = _getData();

    return Container(
      width: double.infinity,
      child: FutureBuilder<DataTableSource>(
          future: _futureData,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return PaginatedDataTable(
                source: snapshot.data!,
                // header: Text('My Products'),
                // headingRowHeight: 0,
                columns: [
                  DataColumn(label: Text('ID')),
                  DataColumn(label: Text('Name')),
                  DataColumn(label: Text('Email')),
                  DataColumn(label: Text('Telephone')),
                  DataColumn(label: Text('Postal Address')),
                  DataColumn(label: Text('Physical Address')),
                  DataColumn(label: Text('Action')),
                ],
                columnSpacing: 100,
                horizontalMargin: 10,
                rowsPerPage: 8,
                showCheckboxColumn: false,
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Padding(
                  padding: EdgeInsets.all(40),
                  child: Text('${snapshot.error}'),
                ),
              );
            } else {
              return Center(
                child: Padding(
                  padding: EdgeInsets.all(40),
                  child: CircularProgressIndicator(),
                ),
              );
            }
          }),
    );
  }
}

class MyData extends DataTableSource {
  // Generate some made-up data
  final List<Map<String, dynamic>> data;

  MyData({this.data = const []});

  bool get isRowCountApproximate => false;
  int get rowCount => data.length;
  int get selectedRowCount => 0;
  DataRow getRow(int index) {
    return DataRow(cells: [
      DataCell(Text(data[index]['id'].toString())),
      DataCell(Text(data[index]["name"])),
      DataCell(Text(data[index]["email"])),
      DataCell(Text(data[index]["telephone"])),
      DataCell(Text(data[index]["postal_address"])),
      DataCell(Text(data[index]["physical_address"])),
      DataCell(
        DropdownButtonFormField(
          value: 'A',
          items: <String>['A', 'B', 'C', 'D'].map((String value) {
            return DropdownMenuItem<String>(
              child: Text(value),
              value: value,
            );
          }).toList(),
          onChanged: (String? val) {
            print(val);
          },
          decoration: InputDecoration(
            border: OutlineInputBorder(),
          ),
        ),
      ),
    ]);
  }
}
