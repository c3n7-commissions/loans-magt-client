import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:file_picker/file_picker.dart';

import 'package:loans_client/shared/authed_route_params.dart';
import 'package:loans_client/shared/utils/show_snackbar.dart';
import 'package:loans_client/shared/utils/validators.dart';
import 'package:loans_client/shared/widgets/base_page_layout.dart';

class CreateCompanyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasePageLayout(
      pageTitle: 'Create Company',
      body: Card(
        child: CompanyCreationForm(),
        elevation: 0.2,
      ),
    );
  }
}

class CompanyCreationForm extends StatefulWidget {
  @override
  _CompanyCreationFormState createState() => _CompanyCreationFormState();
}

class _CompanyCreationFormState extends State<CompanyCreationForm> {
  AuthedRouteParams? args;
  bool _isSubmitting = false;

  final _formKey = GlobalKey<FormState>();
  PlatformFile? _pickedFile;

  Map<String, TextEditingController> _controllers = {
    'name': new TextEditingController(),
    'email': new TextEditingController(),
    'telephone': new TextEditingController(),
    'postal_address': new TextEditingController(),
    'physical_address': new TextEditingController(),
    'logo': new TextEditingController(),
  };

  Future _pickLogo() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.image,
    );

    if (result != null) {
      PlatformFile file = result.files.first;

      _controllers['logo']!.text = file.name;

      setState(() {
        _pickedFile = file;
      });

      print(file.name);
      print(file.bytes);
      print(file.size);
      print(file.extension);
      print(file.path);
    } else {
      // User canceled the picker
    }
  }

  Future _submitForm() async {
    var uri = Uri.parse(
        args!.config['server_address'] + args!.config['company_routes']);
    // TODO: Move this to HttpHelper
    var request = new http.MultipartRequest('POST', uri);
    request.headers['Authorization'] = 'Bearer ' + args!.token;

    _controllers.forEach((key, value) {
      if (key != 'logo') {
        request.fields[key] = value.text;
      }
    });

    String ctype = "";

    switch (_pickedFile!.extension!) {
      case "png":
        ctype = "png";
        break;
      case "jpg":
        ctype = "jpeg";
        break;
      case "jpeg":
        ctype = "jpeg";
        break;
      case "webp":
        ctype = "webp";
        break;
      case "bmp":
        ctype = "bmp";
        break;
      default:
        ctype = "unknown";
        ShowSnackbar.warning(context, "Unknown file type");
        return;
    }

    print("Submittting stuff of type " + ctype);
    if (_pickedFile!.path != null) {
      request.files.add(await http.MultipartFile.fromPath(
          'logo', _pickedFile!.path!,
          contentType: MediaType('image', ctype)));
    } else if (_pickedFile!.bytes != null) {
      request.files
          .add(await http.MultipartFile.fromBytes('logo', _pickedFile!.bytes!));
    } else {
      ShowSnackbar.warning(context, "Could not load image data");
      return;
    }

    var response = await request.send();
    String responseStr = await response.stream.bytesToString();
    print(response.statusCode);
    print(responseStr);
    if (response.statusCode == 201) {
      _controllers.forEach((key, value) {
        value.text = "";
      });

      setState(() {
        _pickedFile = null;
      });
      ShowSnackbar.success(context, "Record saved");
      print("${response.statusCode} : ${responseStr}");
    } else {
      ShowSnackbar.dangerResponse(context, response.statusCode);
      print("${response.statusCode} : ${responseStr}");
    }

    setState(() {
      _isSubmitting = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    args = ModalRoute.of(context)!.settings.arguments as AuthedRouteParams;
    print("Company: " + args!.token);

    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 10, left: 10),
            child: Text(
              'Company Details',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 16,
              ),
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _controllers['name'],
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Name',
                      labelText: 'Name*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _controllers['email'],
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Email',
                      labelText: 'Email*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _controllers['telephone'],
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Telephone',
                      labelText: 'Telephone*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _controllers['postal_address'],
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Postal Address',
                      labelText: 'Postal Address*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _controllers['physical_address'],
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Physical Address',
                      labelText: 'Physical Address*',
                    ),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: TextFormField(
                    controller: _controllers['logo'],
                    readOnly: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Logo',
                        labelText: 'Select Logo*',
                        suffixIcon: IconButton(
                          padding: EdgeInsets.all(10),
                          icon: FaIcon(
                            FontAwesomeIcons.fileImport,
                            size: 14,
                          ),
                          onPressed: () {
                            _pickLogo();
                          },
                        )),
                    validator: Validators.isFilled,
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: ElevatedButton(
              onPressed: () {
                if (_isSubmitting) {
                  return;
                }

                if (_formKey.currentState!.validate()) {
                  // Yay, valid
                  setState(() {
                    _isSubmitting = true;
                  });

                  ShowSnackbar.primary(context, "Saving data");
                  _submitForm();
                }
              },
              child: _isSubmitting
                  ? SizedBox(
                      width: 14,
                      height: 14,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                        strokeWidth: 2,
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Text('Submit'),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
