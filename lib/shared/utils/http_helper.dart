import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:loans_client/shared/authed_route_params.dart';

class HttpHelper {
  static Future sendPostData(AuthedRouteParams args, String routeKey,
      Map<String, dynamic> data) async {
    final response = await http.post(
      Uri.parse(args.config['server_address'] + args.config[routeKey]),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ' + args.token,
      },
      body: jsonEncode(data),
    );

    return response;
  }
}
