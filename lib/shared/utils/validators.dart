import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';

class Validators {
  static String? isPercentage(value) {
    if (value == null || value.isEmpty) {
      return "This field is required";
    }

    double val = double.parse(toNumericString(value, allowPeriod: true));

    if (val < 0 || val > 100) {
      return "Value should be between 0 and 100";
    }

    return null;
  }

  static String? isFilled(value) {
    if (value == null || value.isEmpty) {
      return "This field is required";
    }

    return null;
  }

  static String? isNonStringFilled(value) {
    if (value == null) {
      return "This field is required";
    }

    return null;
  }
}
