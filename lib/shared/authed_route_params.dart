import 'package:loans_client/shared/route_params.dart';

class AuthedRouteParams extends RouteParams {
  final String token;

  AuthedRouteParams(this.token, Map<String, dynamic> config) : super(config);
}
