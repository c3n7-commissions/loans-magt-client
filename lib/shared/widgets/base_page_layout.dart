import 'package:flutter/material.dart';
import 'package:loans_client/shared/widgets/drawer.dart';

class BasePageLayout extends StatelessWidget {
  final String navTitle, pageTitle;
  final Widget body;
  BasePageLayout({
    this.navTitle = "Qubes",
    this.pageTitle = "Change Me",
    this.body = const Text("Change me too"),
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(navTitle),
        elevation: 0.3,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 40),
            child: Tooltip(
              message: 'Log out',
              child: IconButton(
                onPressed: () {},
                icon: Icon(Icons.power_settings_new_rounded),
              ),
            ),
          ),
        ],
      ),
      drawer: NavigationDrawer(),
      body: Padding(
        padding: EdgeInsets.all(70),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10, left: 10, bottom: 20),
              child: Text(
                pageTitle,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 23,
                ),
              ),
            ),
            Container(
              child: body,
            ),
          ],
        ),
      ),
    );
  }
}
