import 'package:flutter/material.dart';
import 'package:loans_client/pages/company/company_listing.dart';
import 'pages/company/create_company.dart';
import 'pages/auth/sign_in.dart';
import 'pages/auth/password_reset.dart';
import 'pages/auth/password_reset_sent.dart';
import 'pages/auth/create_new_password.dart';
import 'pages/landing/splash.dart';
import 'pages/banks/create_lender.dart';
import 'pages/banks/bank_listing.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Qubes',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        cardTheme: CardTheme(
          elevation: 0,
        ),
      ),
      initialRoute: '/sign-in',
      routes: {
        // auth
        '/sign-in': (context) => SignInScreen(),
        '/forgot-password': (context) => PasswordResetScreen(),
        '/reset-email-sent': (context) => ResetEmailSentScreen(),
        '/create-new-password': (context) => CreatePasswordScreen(),
        //
        // -----------
        // landing
        '/splash': (context) => SplashScreen(),
        //
        // ------------
        // Company
        '/create-company': (context) => CreateCompanyScreen(),
        '/company-listing': (context) => CompanyListingScreen(),
        //
        // ------------
        // Lender
        '/create-lender': (context) => CreateLenderScreen(),
        '/lender-listing': (context) => BankListingScreen(),
      },
    );
  }
}
